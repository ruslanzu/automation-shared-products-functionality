package com.algosec.utils.algosaassharedui;

import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIWait;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class UICommon {


    private UIElement button;
    private UIElement title = new UIElement(By.xpath("//title"));
    private UIElement tableWrapper = new UIElement(By.xpath("//*[@ref='eOverlayWrapper']"));
    private UIElement emptyDataWrapper = new UIElement(By.xpath("//*[@ref='noRowsOverlayWrapper']"));

    protected void setButton(String name) {
        button = new UIElement(By.xpath("//button[contains(text(),'" + name + "')]"), name + " button");
    }

    protected void clickButton(String name) {
        setButton(name);
        button.click();
    }

    protected String getTitle() {
        return title.getText();
    }

    @Step("get place holder of text box")
    protected String getPlaceHolder(UIElement inputBox) {
        return inputBox.getElement().getAttribute("placeholder");
    }

    public String getTableWrapperText() {
        return tableWrapper.getText();
    }

    public String getEmptyDataWrapperText() {
        UIWait.waitForPresence(emptyDataWrapper.getLocator(), 5, "empty table wrapper");
        return emptyDataWrapper.getText();
    }
}
