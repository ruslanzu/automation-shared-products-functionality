package com.algosec.utils.algosaassharedui.login;

import com.algosec.utils.algosaassharedui.UIPopUp;
import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIElementList;
import com.algosec.utils.ui.UIWait;
import com.algosec.utils.ui.eConditions;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class CredentialPage extends UIPopUp {

    public CredentialPage() {
        UIWait.wait(eConditions.Visible, contactUsLink, "contact us");
    }

    protected UIElement logo = new UIElement(By.className("login-logo-subtitle"), "logo text");
    protected UIElement contactUsLink = new UIElement(By.xpath("//a[contains(@class,'contact-us-link')]"));
    protected UIElementList labels = new UIElementList(By.xpath("//label[@class='acf-input-label']"), "labels");

    @Step("get Logo")
    public UIElement getLogo() {
        return logo;
    }

    @Step("get Labels")
    public UIElementList getLabels() {
        return labels;
    }

    @Step("get ContactUs Link")
    public UIElement getContactUsLink() {
        return contactUsLink;
    }
}