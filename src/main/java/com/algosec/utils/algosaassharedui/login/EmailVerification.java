package com.algosec.utils.algosaassharedui.login;

import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIWait;
import org.openqa.selenium.By;

public class EmailVerification extends CredentialPage {

    private UIElement title = new UIElement(By.className("email-verification-title"));

    public EmailVerification() {
        UIWait.waitForVisible(title.getLocator(), 5, "email verification page");
    }
}
