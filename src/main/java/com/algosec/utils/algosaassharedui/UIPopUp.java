package com.algosec.utils.algosaassharedui;

import com.algosec.utils.ui.Attachments;
import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIWait;
import com.algosec.utils.ui.eConditions;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class UIPopUp extends UICommon {

    protected UIElement submitButton = new UIElement(By.xpath("//*[@type='submit']"), "submit button");
    protected UIElement cancelButton = new UIElement(By.xpath("//button[contains(@class,'cancel') and contains(text(),'Cancel')]"), "cancel button");
    protected UIElement close = new UIElement(By.className("close"), "x button");
    protected NgSelect ngSelect = new NgSelect();
    protected String selectPath = "(//ng-select)";
    protected UIElement modalTitle = new UIElement(By.className("modal-title"), "title");

    public UIPopUp() {
        Attachments.screenshot("UI popup");
        UIWait.wait(eConditions.Visible, submitButton, "submit button");
    }

    protected UIElement setInput(String name) {
        return new UIElement(By.xpath("//input[@id='" + name + "']"), name + " input");
    }

    @Step("submit")
    public void submit() {
        submitButton.click();
    }

    @Step("cancel")
    public void cancel() {
        cancelButton.click();
    }

    @Step("close")
    public void close() {
        close.click();
    }

    @Step("get title")
    public String getPopUpTitle() {
        return modalTitle.getText();
    }
}