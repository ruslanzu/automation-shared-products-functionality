package com.algosec.utils.algosaassharedui.leftMenu;

import com.algosec.utils.algosaassharedui.asmsIntegration.ASMSIntegrationPage;
import com.algosec.utils.ui.UIElement;
import org.openqa.selenium.By;

public class SettingsMenu extends MainLeftMenu {

    private UIElement accessManagement = new UIElement(By.xpath("//a[contains(@href,'/settings/access-management')]"), "access management icon");
    private UIElement accounts = new UIElement(By.xpath("//a[contains(@href,'/settings/account'])"), "accounts icon");
    private UIElement asmsIntegration = new UIElement(By.xpath("//a[contains(@href,'/settings/asms-integration')]"), "asms integration icon");

    public void clickAccountsIcon() {
        accounts.click();
    }

    public ASMSIntegrationPage clickAsmsIntegrationIcon() {
        if (!isSelected(asmsIntegration))
            asmsIntegration.click();
        return new ASMSIntegrationPage();
    }

    public String getAccessManagementText() {
        return accessManagement.getText();
    }

    public String getAccountsText() {
        return accounts.getText();
    }

    public String getAsmsIntegrationText() {
        return asmsIntegration.getText();
    }

    public UIElement getAccessManagement() {
        return accessManagement;
    }

    public UIElement getAccounts() {
        return accounts;
    }

    public UIElement getAsmsIntegration() {
        return asmsIntegration;
    }
}
