package com.algosec.utils.algosaassharedui;

import com.algosec.utils.algosaassharedui.login.CloudAppsLogin;
import com.algosec.utils.ui.Browser;
import com.algosec.utils.ui.UIElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class UpperMenu {

    private UIElement treeIcon = new UIElement(By.xpath("//*[contains(@class,'navbar-toggle-tree-icon')]"), "tree icon");
    private UIElement menu = new UIElement(By.id("dropdownBasic1"), "drop down");
    private UIElement logout = new UIElement(By.xpath("//button[contains(@class,dropdown-item) and text()='Log Out']"), "logout");
    private UIElement title = new UIElement(By.xpath("//acf-nav-bar//div[contains(@class,'fs-16')]"), "title");
    private UIElement algoLogo = new UIElement(By.className("navbar-algosec-logo"), "algosec logo");
    private UIElement helloUsername = new UIElement(By.xpath("//div[@class='navbar-username']"));
    private UIElement helpCenter = new UIElement(By.xpath("//a[contains(text(),'Help Center')]"), "Help Center");

    @Step("logout")
    public CloudAppsLogin logout() {
        menu.click();
        logout.click();
        return new CloudAppsLogin();
    }

    @Step("logout")
    public CloudAppsLogin logoutIfLoggedIn() {
        if (menu.isDisplayed())
            return logout();
        else return new CloudAppsLogin();
    }

    @Step("open HelpCenter Page")
    public void openHelpCenter() {
        menu.click();
        helpCenter.click();
        Browser.switchTab(1);
    }

    @Step("get title")
    public String getTitle() {
        return title.getText();
    }

    @Step("get username")
    public String getUsername() {
        return helloUsername.getText().split(" ")[1];
    }
}