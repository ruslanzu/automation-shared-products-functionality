package com.algosec.utils.algosaassharedui.asmsIntegration;

import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIWait;
import com.algosec.utils.ui.eConditions;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class ASMSIntegrationPage {

    public ASMSIntegrationPage() {
        UIWait.wait(eConditions.Visible, downloadButton, 10, "download button");
    }

    public static final String ASMS_INTEGRATION_ESTABLISHED_HEADER = "ASMS - CloudFlow trust has been established!";
    public static final String ASMS_INTEGRATION_ESTABLISHED_BODY = "In the event that trust needs to be re-established, download the file again.";
    public static final String ASMS_INTEGRATION_NOT_ESTABLISHED_HEADER = "Have you established trust with ASMS?";
    public static final String ASMS_INTEGRATION_NOT_ESTABLISHED_BODY = "Download the trust establishment file for use in ASMS.";
    public static final String HELP_LINK_TEXT = "For detailed instructions, see the CloudFlow Help Center.";
    public static final String HELP_LINK = "https://docs.algosec.com/en/cloudflow/content/cloud-common/asms-integration.htm";
    public static final String DOWNLOAD_BUTTON_TEXT = "Download";
    public static final String CONFIGURATION_FILE_NAME = "cloudflow_trust_establish_data.zip";
    public static final String UPGRADE_TO_A3210_INFO = "Important: The ASMS version you are using does not support map integration. Upgrade to A32.10 or later to enjoy this functionality.";

    private UIElement connectionHeaderText = new UIElement(By.xpath("//div[@class='header-wrapper']/h3"), "ASMS trust text header");
    private UIElement connectionBodyString1 = new UIElement(By.xpath("//div[contains(@class, 'body-wrapper')]/h3[1]"), "Asms page body text String1");
    private UIElement connectionBodyString2 = new UIElement(By.xpath("//div[contains(@class, 'body-wrapper')]/h3[2]"), "Asms page body text String2");
    private UIElement downloadButton = new UIElement(By.xpath("//button[@class='btn btn-primary btn-default']"), "download configuration zip file button");
    private UIElement getHelpCenterLinkText = new UIElement(By.xpath("//div[contains(@class, 'form-additional-description')]"), "cloudflow help center link text");
    private UIElement helpCenterLink = new UIElement(By.xpath("//div[@class='form-additional-description']/a"), "cloudflow help center link");

    @Step("get connection header text")
    public String getConnectionHeaderText() {
        return connectionHeaderText.getText();
    }

    @Step("get connection body text string 1")
    public String getConnectionBodyString1() {
        return connectionBodyString1.getText();
    }

    @Step("get body text second string")
    public String getConnectionBodyString2() {
        return connectionBodyString2.getText();
    }

    @Step("get download button text")
    public String getDownloadButtonText() { return downloadButton.getText(); }

    @Step("click download button")
    public void clickDownloadButton() {
        downloadButton.click();
    }

    @Step("get help center link URL")
    public String getHelpCenterLinkURL() {
        return helpCenterLink.getElementLink();
    }

    @Step("get help center link text")
    public String getHelpCenterLinkText() {
        return getHelpCenterLinkText.getText();
    }

}
