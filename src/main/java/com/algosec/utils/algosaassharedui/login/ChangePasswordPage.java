package com.algosec.utils.algosaassharedui.login;

import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIElementList;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class ChangePasswordPage extends CredentialPage {

    private UIElement title = new UIElement(By.className("change-password-title"));
    private UIElement newPassword = new UIElement(By.xpath("//input[@id='newPassword']"));
    private UIElement newPasswordVerification = new UIElement(By.xpath("//input[@id='newPasswordVerification']"));
    private UIElement Email = new UIElement(By.xpath("//input[@id='email']"));
    private UIElement verificationCodeInput = new UIElement(By.xpath("//input[@id='code']"));
    private UIElementList tooltips = new UIElementList(By.xpath("//div[@class='tooltip-inner']"), "tooltips");

    @Step("change password for new user")
    public ChangePasswordPage fillForm(String newPass, String verifyPass, String email) {
        Email.pasteText(email);
        newPassword.pasteText(newPass);
        newPasswordVerification.pasteText(verifyPass);
        return this;
    }

    @Step("change password for existing user")
    public ChangePasswordPage fillFormExistingUser(String newPass, String verifyPass, String verificationCode) {
        verificationCodeInput.pasteText(verificationCode);
        newPassword.pasteText(newPass);
        newPasswordVerification.pasteText(verifyPass);
        return this;
    }

    @Step("Fill change password form")
    public ChangePasswordPage fillForm(String newPass, String verifyPass) {
        newPassword.pasteText(newPass);
        newPasswordVerification.pasteText(verifyPass);
        return this;
    }

    @Step("click update passowrd")
    public EmailVerification Submit() {
        submitButton.click();
        return new EmailVerification();
    }

    @Step("get Change Password Page Title")
    public UIElement getTitleChangePasswordPage() {
        return title;
    }

    @Step("get tooltip")
    public UIElementList getTooltips() {
        return tooltips;
    }
}