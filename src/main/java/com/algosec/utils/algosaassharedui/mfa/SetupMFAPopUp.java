package com.algosec.utils.algosaassharedui.mfa;

import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIWait;
import com.algosec.utils.ui.eConditions;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class SetupMFAPopUp {

    private UIElement activateMFA = new UIElement(By.xpath("//button[text()=' Activate MFA ']"), "Activate MFA Button");
    private UIElement authenticationCode = new UIElement(By.xpath("//input[@id='userCode']"), "Authentication Code Input Box");
    private UIElement qrCode = new UIElement(By.xpath("//img[contains(@src,'data:image/png;base64')]"), "QR Code Image");

    public SetupMFAPopUp() {
        UIWait.wait(eConditions.Visible, qrCode, "QR Image");
    }

    @Step("Input MFA code")
    public SetupMFAPopUp inputUserCode(String userCode) {
        authenticationCode.sendKeys(userCode);
        return this;
    }

    @Step("submit")
    public void submit() {
        activateMFA.click();
    }

    @Step("Check page existence")
    public boolean isImageDisplayed() {
        return qrCode.isDisplayed();
    }
}
