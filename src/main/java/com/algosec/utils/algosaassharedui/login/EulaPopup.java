package com.algosec.utils.algosaassharedui.login;

import com.algosec.utils.algosaassharedui.UIPopUp;
import com.algosec.utils.algosaassharedui.home.HomePage;
import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIWait;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class EulaPopup extends UIPopUp {

    private UIElement title = new UIElement(By.className("eula-modal-title-flow"));

    public EulaPopup() {
        UIWait.waitForVisible(title.getLocator(), 5, "eula pop up");
    }

    public EulaPopup(boolean optional) {
        if (!optional)
            UIWait.waitForVisible(title.getLocator(), 5, "eula pop up");
    }

    @Step("sign")
    public HomePage sign() {
        submit();
        return new HomePage();
    }

    @Step("decline")
    public CloudAppsLogin decline() {
        clickButton("Decline");
        return new CloudAppsLogin();
    }
}
