package com.algosec.utils.algosaassharedui;

import com.algosec.utils.algosaassharedui.leftMenu.MainLeftMenu;
import com.algosec.utils.ui.*;
import org.openqa.selenium.By;

public class CloudAppsMainPage {

    private UIElement sideBar = new UIElement(By.className("sidebar"), "sidebar");
    private UIElement title = new UIElement(By.xpath("//div[contains(@class,'navbar-title')]"), "title");
    private UIElement settingsItem = new UIElement(By.xpath("//*[contains(@href,'/settings/')]/span"), "Settings Item");
    private UIElement settingsIcon = new UIElement(By.xpath("//div[@routerlinkactive='settings-active']"), "Settings Icon");

    public CloudAppsMainPage() {
        if (!UIWait.waitWithoutFailure(settingsIcon)) {
            Browser.refresh();
            UIWait.wait(eConditions.Presence, sideBar, "sidebar");
        }
        Attachments.screenshot("main page");
    }

    public MainLeftMenu getMainLeftMenu() {
        return new MainLeftMenu();
    }

    public UIElement getTitle() {
        return title;
    }

    public UIElement getSettingsItem() {
        settingsItem.hoverToElement();
        return settingsItem;
    }

}
