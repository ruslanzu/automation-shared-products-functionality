package com.algosec.utils.algosaassharedui.login;

import com.algosec.utils.ui.UIElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class ForgotPasswordPage extends CredentialPage {

    private UIElement title = new UIElement(By.className("forgot-password-title"));
    private UIElement userNameInput = new UIElement(By.xpath("//input[@id='username']"), "user name");

    @Step("fill forgot password form")
    public ForgotPasswordPage enterUserNameClickResetPassword(String userName) {
        userNameInput.pasteText(userName);
        return this;
    }

    @Step("submit forgot password form")
    public ChangePasswordPage submitForm() {
        super.submit();
        return new ChangePasswordPage();
    }

    @Step("get forgot password title ")
    public UIElement getTitleForgotPassword() {
        return title;
    }
}