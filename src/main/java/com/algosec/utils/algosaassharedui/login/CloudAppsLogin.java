package com.algosec.utils.algosaassharedui.login;

import com.algosec.utils.algosaassharedui.CloudAppsMainPage;
import com.algosec.utils.algosaassharedui.mfa.MFAValidate;
import com.algosec.utils.common.TenantHandling;
import com.algosec.utils.email.EmailHelper;
import com.algosec.utils.email.MailBoxManager;
import com.algosec.utils.email.MessageInfo;
import com.algosec.utils.report.Reporter;
import com.algosec.utils.ui.Attachments;
import com.algosec.utils.ui.UIElement;
import io.qameta.allure.Step;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

public class CloudAppsLogin extends CredentialPage {

    private UIElement tenantIdInput = new UIElement(By.xpath("//input[@id='tenantId']"), "tenant id");
    private UIElement userNameInput = new UIElement(By.xpath("//input[@id='username']"), "user name");
    private UIElement errorMessage = new UIElement(By.xpath("//div[contains(@class,'login-error')]"), "login error");
    private UIElement passwordInput = new UIElement(By.xpath("//input[@type='password']"), "password");
    private UIElement signtitle = new UIElement(By.className("sign-in-title"), "sign in title");
    private UIElement registerText = new UIElement(By.className("register-text"));
    private UIElement registerLink = new UIElement(By.xpath("//a[contains(@class,'register-link')]"));
    private UIElement forgotPassword = new UIElement(By.xpath("//acf-input[@label='Password']/../a"));

    @Step("is loggedIn")
    public boolean isLoggedout() {
        return tenantIdInput.isDisplayed();
    }

    @Step("Login")
    public CloudAppsMainPage login(String tenant, String user, String password) {
        fillForm(tenant, user, password);
        return new CloudAppsMainPage();
    }

    @Step("Login")
    public MFAValidate loginWithMFA(String tenant, String user, String password) {
        fillForm(tenant, user, password);
        return new MFAValidate();
    }

    @Step("Login")
    public CloudAppsLogin badLogin(String tenant, String user, String password) {
        fillForm(tenant, user, password);
        return new CloudAppsLogin();
    }

    @Step("Login")
    public ChangePasswordPage firstLogin(String tenant, String user, String password) {
        fillForm(tenant, user, password);
        Attachments.screenshot("login page");
        return new ChangePasswordPage();
    }

    @Step("Login")
    public EulaPopup firstLoginAfterChangePasseword(String tenant, String user, String password) {
        fillForm(tenant, user, password);
        return new EulaPopup();
    }

    private void fillForm(String tenant, String user, String password) {
        tenantIdInput.sendKeys(tenant);
        userNameInput.sendKeys(user);
        passwordInput.sendKeys(password);
        submitButton.click();
    }

    @Step("click forgot password")
    public ForgotPasswordPage clickForgotPassword() {
        forgotPassword.click();
        return new ForgotPasswordPage();
    }

    @Step("change user password")
    public String changeUserPassword(String username, String newPassword, String emailAddress) {
        MessageInfo[] allCurrentEmails = MailBoxManager.getAllMails(emailAddress);
        ChangePasswordPage changePasswordPage = clickForgotPassword().enterUserNameClickResetPassword(username).submitForm();
        String lastEmailBody = EmailHelper.getLastEmailBody(emailAddress, allCurrentEmails);
        String verificationCode = StringUtils.substringBetween(lastEmailBody, "enter the following verification code in CloudFlow: <b>", "</b>");
        Reporter.getInstance().log("Verification code: " + verificationCode + "\nLogin params:\nuserName:" + username + "\npassword:" + newPassword + "\ntenant:" + TenantHandling.getTenant(getClass().getSimpleName()));
        changePasswordPage.fillFormExistingUser(newPassword, newPassword, verificationCode).submit();
        return newPassword;
    }

    @Step("get SignTitle")
    public UIElement getSignTitle() {
        return signtitle;
    }

    @Step("get ErrorMessage Element")
    public UIElement getErrorMessage() {
        return errorMessage;
    }

    @Step("get RegisterText Element")
    public UIElement getRegisterText() {
        return registerText;
    }

    @Step("get RegisterLink")
    public UIElement getRegisterLink() {
        return registerLink;
    }

    @Step("get ForgotPassword Button")
    public UIElement getForgotPassword() {
        return forgotPassword;
    }

}