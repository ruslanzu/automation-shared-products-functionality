package com.algosec.utils.algosaassharedui.authtester;

import com.algosec.utils.algosaassharedui.login.CloudAppsLogin;
import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIWait;
import com.algosec.utils.ui.eConditions;
import org.openqa.selenium.By;

public class AuthTesterPage {

    public AuthTesterPage() {
        UIWait.wait(eConditions.Visible, buttonsContainer, 10, "UI component with buttons");
    }

    private UIElement buttonsContainer = new UIElement(By.xpath("//div[contains(@class, 'app-main-content')]"));

    public CloudAppsLogin clickLogoutButton(){
        new UIElement(By.xpath("//button[text()='logout']")).click();
        return new CloudAppsLogin();
    }

}
