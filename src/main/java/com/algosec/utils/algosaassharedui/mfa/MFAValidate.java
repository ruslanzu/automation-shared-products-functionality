package com.algosec.utils.algosaassharedui.mfa;

import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIWait;
import com.algosec.utils.ui.eConditions;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class MFAValidate {

    private UIElement submitButton = new UIElement(By.xpath("//button[text()=' Submit and login ']"), "Activate MFA Button");
    private UIElement mfaCode = new UIElement(By.xpath("//input[@id='userCode']"), "MFA Code Input Box");
    private UIElement algosecImage = new UIElement(By.id("ic_algoseccloud"), "Algosec Cloud Image");

    public MFAValidate() {
        UIWait.wait(eConditions.Visible, mfaCode, "Algosec Cloud Image");
    }

    @Step("Input MFA code")
    public MFAValidate inputUserCode(String userCode) {
        mfaCode.sendKeys(userCode);
        return this;
    }

    @Step("submit")
    public void submit() {
        submitButton.click();
    }
}