package com.algosec.utils.algosaassharedui;

import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIElementList;
import com.algosec.utils.ui.UIWait;
import com.algosec.utils.ui.eConditions;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NgSelect {

    @Step("select {1} from box {0")
    public void select(String path, String option) {
        UIElement selectBox = new UIElement(By.xpath(path), "select box");
        selectBox.click();
        getOption(option).click();
    }

    public void selectMultipleValues(String path, String[] option) {
        for (int i = 0; i < option.length; i++) {
            select(path, option[i]);
        }
    }

    @Step("select option from list {1}")
    public void select(UIElement selectBox, String option) {
        selectBox.click();
        getOption(option).click();
    }

    @Step("select option from list {1}")
    public void select(UIElement selectBox, String option, String path) {
        selectBox.click();
        getOption(option, path).click();
    }

    public void selectMultipleValues(UIElement selectBox, String[] option) {
        for (int i = 0; i < option.length; i++)
            select(selectBox, option[i]);
    }

    public UIElement getOption(String option) {
        UIElement optionInList = new UIElement(By.xpath("//*[(contains(@class,'ng-option-label')or contains(@class,'ng-option')) and contains(text(),'" + option + "')]"), option);
        UIWait.wait(eConditions.Visible, optionInList, 10, "option " + option);
        return optionInList;
    }

    public String[] getOptions(String formControlName){
        return new UIElementList(By.cssSelector("ng-select[formcontrolname='" + formControlName + "'] .ng-value-label"), "All options values").getTexts();
    }

    public UIElement getOption(String option, String path) {
        UIElement optionInList = new UIElement(By.xpath("//*[(contains(@class,'ng-option-label')or contains(@class,'ng-option'))]" + path + "[contains(text(),'" + option + "')]"), option);
        UIWait.wait(eConditions.Visible, optionInList, 10, "option " + option);
        return optionInList;
    }

    public String[] getOptions() {
        UIElementList options = new UIElementList(By.xpath("//span[contains(@class, 'ng-option-label')]"), "options presented at selector");
        return options.getTexts();
    }

    public List<String> getPolicyOptions() {
        new UIElement(By.xpath("(//div[@class='ng-input'])[6]")).click();
        UIElementList options = new UIElementList(By.xpath("//*[contains(@class, 'ng-option')]"), "options presented at selector");
        return Arrays.asList(options.getTexts()).stream().filter(x -> !x.equals("All")).collect(Collectors.toList());//removing default "All" value from the List
    }

    @Step("type and select - {0}")
    public void typeAndSelect(UIElement input, String typeVal, String optionText) {
        input.pasteText(typeVal);
        getOption(optionText);
        input.sendKeys(Keys.ENTER);
    }

    @Step("type and select from checkbox - {0")
    public void typeAndSelectCheckBox(UIElement input, String typeVal, String optionText) {
        input.pasteText(typeVal);
        getOption(optionText);
        new UIElement(By.xpath("//input[@type='checkbox']/../../div[contains(text(),'" + optionText + "')]")).click();
    }

    public void typeAndSelectMultipleValues(UIElement input, List typeVal, List optionText) {
        for (int i = 0; i < typeVal.size(); i++)
            typeAndSelect(input, typeVal.get(i).toString(), optionText.get(i).toString());
    }

    public void typeAndSelectFromCheckboxMultipleValues(UIElement input, List<String> typeVal, List<String> optionText) {
        for (int i = 0; i < typeVal.size(); i++)
            typeAndSelectCheckBox(input, typeVal.get(i), optionText.get(i));
    }

    @Step("click select arrow")
    public void clickSelectArrow() {
        new UIElement(By.xpath("//ng-select[@placeholder='Select']//span[@class='ng-arrow-wrapper']"), "close select box").click();
    }
}
