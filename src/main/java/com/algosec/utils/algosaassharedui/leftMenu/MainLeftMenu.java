package com.algosec.utils.algosaassharedui.leftMenu;

import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIElementList;
import com.algosec.utils.ui.WebDriverFactory;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

public class MainLeftMenu {

    protected UIElement settingIcon = new UIElement(By.xpath("//*[contains(@class,'settings-icon')]"), "setting icon");
    protected UIElement homeIcon = new UIElement(By.xpath("//a[@href='/']"), "home icon");
    protected UIElement cloudFlowLogo = new UIElementList(By.className("sidebar-header-text"), "CloudFlow Logo");
    private UIElement authTesterLink = new UIElementList(By.xpath("//a[@href='/lib-tester/auth-tester']"), "Auth tester link");

    private UIElement getLeftMenuOption(String buttonText) {
        return new UIElement(By.xpath("//*[(@class='sidebar-section-button-text') and (text()='" + buttonText + "')]"), buttonText);
    }

    @Step("open home page")
    public MainLeftMenu openHomeFromIcon() {
        clickIcon("");
        return this;
    }

    @Step("open settings menu")
    public SettingsMenu openSettingsMenu() {
        hoverToElement(settingIcon);
        return new SettingsMenu();
    }

    @Step("click to auth tester icon")
    public void clickAuthTester() {
        hoverToElement(authTesterLink);
        authTesterLink.click();
    }

    @Step("hover to element")
    protected void hoverToElement(UIElement element) {
        Actions actions = WebDriverFactory.getWebDriverActions();
        actions.moveToElement(element.getElement()).perform();
    }

    public String getCloudFlowLogo() {
        hoverToElement(homeIcon);
        return cloudFlowLogo.getText();
    }

    private void clickIcon(String name) {
        new UIElement(By.xpath("//a[@href='/" + name + "']")).click();
    }

    protected boolean isSelected(UIElement element) {
        String classAttribute = element.getElement().getAttribute("class");
        return classAttribute.contains("selected");
    }
}
