package com.algosec.utils.algosaassharedui.home;

import com.algosec.utils.ui.UIElement;
import com.algosec.utils.ui.UIElementList;
import com.algosec.utils.ui.UIWait;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class HomePage {

    public HomePage() {
        UIWait.waitForVisible(subTitle.getLocator(), 5, "sub title");
    }

    private UIElementList cloudFlowText = new UIElementList(By.xpath("//div[@class='home-wrapper']//div[contains(@class,'title')]//span"), "");
    private UIElement subTitle = new UIElementList(By.xpath("//div[@class='home-wrapper']//div[contains(@class,'sub-title')]"), "sub title");
    private UIElementList welcomeText1 = new UIElementList(By.xpath("//div[@class='home-wrapper']//div[contains(@class,'welcome-block')]/div/div/*"), "");
    private UIElement welcomeText2 = new UIElement(By.xpath("//div[@class='home-wrapper']//div[contains(@class,'welcome-block')]//div[2]"));
    private UIElement networkPolicyText1 = new UIElement(By.xpath("//div[@class='home-wrapper']//div[contains(@class,'define-new-policy-block')]"));
    private UIElementList networkPolicyText2 = new UIElementList(By.xpath("//div[@class='home-wrapper']//div[contains(@class,'define-new-policy-block')]//*"), "");
    private static UIElement homeText = new UIElement(By.xpath("//div[text()='Home']"), "Home text");

    @Step("is Home Page")
    public static boolean isHomePage() {
        return homeText.isDisplayed();
    }

    @Step("get 'CloudFlow' text")
    public String[] getCloudFlowText() {
        return cloudFlowText.getTexts();
    }

    @Step("get policy text")
    public String[] getPolicyText() {
        return networkPolicyText2.getTexts();
    }

    @Step("get 'welcome' text1")
    public String[] getWelcomeText() {
        return welcomeText1.getTexts();
    }

    @Step("get 'welcome' text2")
    public String getWelcomeText2() {
        return welcomeText2.getText();
    }

    @Step("get 'CloudFlow' text")
    public String getSubTitle() {
        return subTitle.getText();
    }

}
